#pragma once

#include <NumpyArray.hpp>
#include <OS_utils.hpp>

extern "C" {
	EXPORT int region_based(struct NumpyArray *, struct NumpyArray *, struct NumpyArray *, struct NumpyArray *,
		struct NumpyArray *);
}
