#include <region_based.h>
#include <Image.hpp>
#include <cstdint>
#include <stack>
#include <tuple>

bool isInSameRegion(std::pair<size_t, size_t> current, std::pair<size_t, size_t> other,
	const Image<uint8_t, false> &imSuperPixel, const Image<uint8_t, false> &imRegionGrowing,
	const Image<uint8_t, false> &imMeanShift, const Image<uint8_t, false> &imGraphBased) {

	/* Look for RGB at current position */
	auto rCurrent = std::make_tuple(std::get<0>(current), std::get<1>(current), 0),
		 gCurrent = std::make_tuple(std::get<0>(current), std::get<1>(current), 1),
		 bCurrent = std::make_tuple(std::get<0>(current), std::get<1>(current), 2),
		 rOther = std::make_tuple(std::get<0>(other), std::get<1>(other), 0),
		 gOther = std::make_tuple(std::get<0>(other), std::get<1>(other), 1),
		 bOther = std::make_tuple(std::get<0>(other), std::get<1>(other), 2);

	bool isSameSuperPixel = (imSuperPixel(rCurrent) == imSuperPixel(rOther) &&
		imSuperPixel(gCurrent) == imSuperPixel(gOther) && imSuperPixel(bCurrent) == imSuperPixel(bOther));

	bool isSameRegionGrowing = (imRegionGrowing(rCurrent) == imRegionGrowing(rOther) &&
		imRegionGrowing(gCurrent) == imRegionGrowing(gOther) && imRegionGrowing(bCurrent) == imRegionGrowing(bOther));

	bool isSameMeanShift = (imMeanShift(rCurrent) == imMeanShift(rOther) &&
		imMeanShift(gCurrent) == imMeanShift(gOther) && imMeanShift(bCurrent) == imMeanShift(bOther));

	bool isSameGraphBased = (imGraphBased(rCurrent) == imGraphBased(rOther) &&
		imGraphBased(gCurrent) == imGraphBased(gOther) && imGraphBased(bCurrent) == imGraphBased(bOther));

	if (!isSameMeanShift && !isSameGraphBased)
		return false;

	if (isSameSuperPixel && isSameRegionGrowing)
		return true;

	return false;
}

int region_based(struct NumpyArray *npSuperPixel, struct NumpyArray *npRegionGrowing, struct NumpyArray *npMeanShift,
	struct NumpyArray *npGraphBased, struct NumpyArray *npResult) {

	Image<uint8_t, false> imSuperPixel(npSuperPixel), imRegionGrowing(npRegionGrowing), imMeanShift(npMeanShift),
		imGraphBased(npGraphBased);
	Image<int32_t, false> imResult(npResult);

	size_t height = imResult.getHeight(), width = imResult.getWidth();
	/* set initial values of the resulting image to -1 (no region) */
	for(size_t i = 0; i < height; i++) {
		for(size_t j = 0; j < width; j++) {
			imResult(i, j) = -1;
		}
	}

	int currentLabel = 0;
	std::stack<std::pair<size_t, size_t>> s;

	for(size_t i = 0; i < height; i++) {
		for(size_t j = 0; j < width; j++) {
			if(imResult(i, j) == -1) {
				s.push(std::make_pair(i, j));

				while(s.size() > 0) {
					auto currentPixel = s.top();
					imResult(currentPixel) = currentLabel;
					s.pop();

					auto topNeighbour = std::make_pair(std::get<0>(currentPixel) - 1, std::get<1>(currentPixel));
					auto bottomNeighbour = std::make_pair(std::get<0>(currentPixel) + 1, std::get<1>(currentPixel));
					auto leftNeighbour = std::make_pair(std::get<0>(currentPixel), std::get<1>(currentPixel) - 1);
					auto rightNeighbour = std::make_pair(std::get<0>(currentPixel), std::get<1>(currentPixel) + 1);

					if(std::get<0>(currentPixel) > 0 && imResult(topNeighbour) == -1 && isInSameRegion(currentPixel,
						topNeighbour, imSuperPixel, imRegionGrowing, imMeanShift, imGraphBased)) {
						s.push(topNeighbour);
					}

					if(std::get<0>(currentPixel) < height - 1 && imResult(bottomNeighbour) == -1 && isInSameRegion(
						currentPixel, bottomNeighbour, imSuperPixel, imRegionGrowing, imMeanShift, imGraphBased)) {
						s.push(bottomNeighbour);
					}

					if(std::get<1>(currentPixel) > 0 && imResult(leftNeighbour) == -1 && isInSameRegion(currentPixel,
						leftNeighbour, imSuperPixel, imRegionGrowing, imMeanShift, imGraphBased)) {
						s.push(leftNeighbour);
					}

					if(std::get<1>(currentPixel) < width - 1 && imResult(rightNeighbour) == -1 && isInSameRegion(
						currentPixel, rightNeighbour, imSuperPixel, imRegionGrowing, imMeanShift, imGraphBased)) {
						s.push(rightNeighbour);
					}
				}
				currentLabel += 1;
			}
		}
	}

	return 0;
}