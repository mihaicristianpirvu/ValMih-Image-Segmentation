#ifndef NUMPY_ARRAY_HPP
#define NUMPY_ARRAY_HPP

#include <cstddef>

enum NumpyArrayType {
	NP_UINT8 = 0,
	NP_INT32,
	NP_INT64,
	NP_FLOAT32,
	NP_FLOAT64
};

/* type is handled from Python by calling the correct function. ELSE BUGS. */
struct NumpyArray {
	char *data;
	size_t *shape;
	size_t shapeSize;
	enum NumpyArrayType type;
};

#endif /* NUMPY_ARRAY_HPP */
