#include <utils/RegionGrowing.h>
#include <images/Image.h>


int region_growing(struct NumpyArray *inputImage, struct NumpyArray *resultImage, int threshold, bool randomSeeds, bool randomColors)
{
	bool grayscale = inputImage->shape[2] == 1 ? true : false;
	Image image(inputImage->shape[1], inputImage->shape[0], (unsigned char*)inputImage->data, grayscale);

	/* apply Region Growing image segmentation algorithm */
	image.segment(ImageAlgorithm::SEGMENTATION_REGION_GROWING, threshold, randomSeeds, randomColors);

	/* set the result */
	for (size_t i = 0; i < image.getPixels().size(); i++)
	{
		resultImage->data[i * 3] = image.getPixels()[i].getRed();
		resultImage->data[i * 3 + 1] = image.getPixels()[i].getGreen();
		resultImage->data[i * 3 + 2] = image.getPixels()[i].getBlue();
	}
	resultImage->shape[0] = image.getHeight();
	resultImage->shape[1] = image.getWidth();
	resultImage->shape[2] = image.isGrayscale() ? 1 : 3;
	resultImage->shapeSize = 3;
	resultImage->type = NP_UINT8;

	return 0;
}
