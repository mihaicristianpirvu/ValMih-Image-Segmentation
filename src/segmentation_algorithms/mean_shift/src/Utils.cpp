#include "Utils.h"

double ComputeDistance(Vec3b from, Vec3b to)
{
	return sqrt(pow(from.val[0] - to.val[0], 2) + pow(from.val[1] - to.val[1], 2) + pow(from.val[2] - to.val[2], 2));
}

double ComputeImageMean(Mat image, bool convert)
{
	float sum = 0;
	Mat imgGray = convert ? ConvertImageToGray(image) : image;
	for (size_t i = 0; i < (size_t)imgGray.rows; i++)
	{
		for (size_t j = 0; j < (size_t)imgGray.cols; j++)
		{
			sum += convert ? imgGray.at<uchar>(i, j) : imgGray.at<float>(i, j);
		}
	}
	return sum / (imgGray.rows*imgGray.cols);
}

double ComputeImageStandardDeviation(Mat image)
{
	float mean = ComputeImageMean(image, true);
	Mat imgGray = ConvertImageToGray(image);
	Mat differences = Mat(Size(image.cols, image.rows), CV_32FC1);
	for (size_t i = 0; i < (size_t)image.rows; i++)
	{
		for (size_t j = 0; j < (size_t)image.cols; j++)
		{
			differences.at<float>(i, j) = pow((float)imgGray.at<uchar>(i, j) - mean, 2);
		}
	}
	float meanDif = ComputeImageMean(differences, false);
	return sqrt(meanDif);
}

Mat ConvertImageToGray(Mat image)
{
	Mat imgGray = Mat(Size(image.cols, image.rows), CV_8UC1);
	cvtColor(image, imgGray, COLOR_BGR2GRAY);
	return imgGray;
}

Mat NumpyArrayToMat(NumpyArray* in)
{
	uint8_t *data = (uint8_t *)in->data;
	if (in->shapeSize == 3)
	{
		Mat out = Mat(Size(in->shape[1], in->shape[0]), CV_8UC3);
		for (size_t i = 0; i < (size_t)out.rows; i++)
		{
			for (size_t j = 0; j < (size_t)out.cols; j++)
			{
				int index = i * out.cols * 3 + j * 3;
				out.at<Vec3b>(i, j) = {data[index+2], data[index+1], data[index]};
			}
		}
		return out;
	}
	return Mat();
}

void MatToNumpyArray(Mat in, struct NumpyArray *out)
{
	uint8_t *data = (uint8_t *)out->data;
	for (size_t i = 0; i < (size_t)in.rows; i++)
	{
		for (size_t j = 0; j < (size_t)in.cols; j++)
		{
			int index = i*in.cols*3 + j*3;
			data[index + 2] = in.at<Vec3b>(i, j).val[0];
			data[index + 1] = in.at<Vec3b>(i, j).val[1];
			data[index + 0] = in.at<Vec3b>(i, j).val[2];
		}
	}
}

int mean_shift(struct NumpyArray* image, struct NumpyArray* outputImage, double spatialRadius, double colorRadius, int maxLevel)
{
	Mat img = NumpyArrayToMat(image);
	MeanShift meanShift;

	meanShift.ApplyMeanShift(img, spatialRadius, colorRadius, maxLevel);
	MatToNumpyArray(meanShift.Result(), outputImage);
	return 0;
}