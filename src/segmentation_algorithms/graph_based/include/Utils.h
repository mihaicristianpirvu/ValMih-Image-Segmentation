#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <NumpyArray.hpp>
#include <OS_utils.hpp>

using namespace std;
using namespace cv;

#include "DisjointSet.h"
#include "GraphBased.h"

extern "C" {
	EXPORT int graph_based(struct NumpyArray *, struct NumpyArray *, int, float);
}

double ComputeDistance(Vec3b from, Vec3b to);
double ComputeImageMean(Mat image);
double ComputeImageStandardDeviation(Mat image);
Mat ConvertImageToGray(Mat image);
Mat NumpyArrayToMat(NumpyArray* in);
NumpyArray* MatToNumpyArray(Mat in);
